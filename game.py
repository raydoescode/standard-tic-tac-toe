def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, space_number):
    print_board(board)
    print("GAME OVER")
    print(board[space_number], "has won")
    exit()

def is_row_winner(board):
    index = 0
    is_winner = False
    # The variable _ is not being used in the for loop but must exist for python to execute
    for _ in board:
        # finding row index
        if index % 3 == 0:
            # comparing each entry in row to see if they are equal
            if board[index] == board[index + 1] and board[index + 1] == board[index + 2]:
                is_winner = True
        index += 1
    return is_winner

def is_column_winner(board):
    index = 0
    is_winner = False
    for _ in board:
        if index in [0, 1, 2]:
            if board[index] == board[index + 3] and board[index + 3] == board[index + 6]:
                is_winner = True
        index += 1
    return is_winner

def is_diagonal_winner(board):
    index = 0
    is_winner = False
    for _ in board:
        if index in [0]:
            if board[index] == board[index + 4] and board[index + 4] == board[index + 8]:
                is_winner = True
        elif  index in [2]:
            if board[index] == board[index + 2] and board[index + 2] == board[index + 4]:
                is_winner = True
        index += 1
    return is_winner

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1,10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board):
        game_over(board, space_number)
    elif is_column_winner(board):
        game_over(board, space_number)
    elif is_diagonal_winner(board):
        game_over(board, space_number)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
